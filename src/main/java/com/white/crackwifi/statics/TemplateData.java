package com.white.crackwifi.statics;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * @author huangzy
 * @Description: 模板静态数据
 * @date 2019/12/24 15:42
 */
public class TemplateData {

    private TemplateData(){}

    //SSID模板数据
    private static File ssidTemplateFile;

    //密码数据
    private static List<String> passwordData;

    public static File getSsidTemplateFile() {
        return ssidTemplateFile;
    }

    public static void setSsidTemplateFile(File ssidTemplateFile) {
        TemplateData.ssidTemplateFile = ssidTemplateFile;
    }

    public static List<String> getPasswordData() {
        //随机打乱顺序, 避免每次执行,都执行前面几个密码
        Collections.shuffle(passwordData);
        return passwordData;
    }

    public static void setPasswordData(List<String> passwordData) {
        TemplateData.passwordData = passwordData;
    }
}
