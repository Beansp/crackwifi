package com.white.crackwifi.statics;

/**
 * @author huangzy
 * @Description: 执行语句枚举
 * @date 2019/12/24 14:50
 */
public enum ExecuteCmdEnum {

    //列出所有可用wifi
    GET_ALL("netsh wlan show networks mode=bssid"),

    //添加配置文件
    ADD_PROFILE("netsh wlan add profile filename=FILE_NAME"),

    //连接WIFI
    JOIN_WIFI("netsh wlan connect name=SSID_NAME"),

    //删除WIFI配置文件 (避免保存错误密码)
    DELETE_PROFILE("netsh wlan delete profile name=FILE_NAME"),

    //导出配置文件
    export_PROFILE("netsh wlan export profile key=clear");

    private String cmd;

    ExecuteCmdEnum(String cmd){
        this.cmd = cmd;
    }

    public String getCmd() {
        return cmd;
    }
}
