package com.white.crackwifi.utils;

import java.util.regex.Pattern;

/**
 * @author huangzy
 * @Description: 数字工具类
 * @date 2019/12/25 16:49
 */
public class NumberUtil {

    private NumberUtil(){}

    /**
     * 校验是否为纯数字
     * @param o
     * @return
     */
    public static boolean isNumber(Object o){
        return (Pattern.compile("[0-9]*")).matcher(String.valueOf(o)).matches();
    }

}
