package com.white.crackwifi.exec;

import com.white.crackwifi.model.Ssid;

import java.util.List;

/**
 * @author huangzy
 * @Description: 破解WIFI顶层接口
 * @date 2019/12/24 15:52
 */
public interface CrackWifi {

    /**
     * 获取电脑周围的所有可搜索到的WIFI
     * @return WIFI集合
     */
    List<Ssid> getSsidList();

    /**
     * 添加配置文件到wifi配置中
     * @param fileName 文件名称
     */
    boolean addProfile(String fileName);

    /**
     * 删除WIFI配置文件
     * @param fileName 文件名称
     */
    void deleteProfile(String fileName);

    /**
     * 连接WIFI
     * @param SSID WIFI名称
     * @return 是否连接成功
     */
    boolean connect(String SSID);

    /**
     * Ping外网IP
     * @return 是否Pingd通
     */
    boolean ping();

    /**
     * 校验WIFI密码是否正确
     * @param ssid WIFI名称
     * @param password 密码
     * @return
     */
    boolean check(Ssid ssid,String password);

}
