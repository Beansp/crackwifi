package com.white.crackwifi.exec;

/**
 * @author huangzy
 * @Description: 开始破解WIFI入口
 * @date 2019/12/24 16:59
 */
public interface StartCrackWifi {

    /**
     * 开始循环破解WIFI
     */
    void start();

}
