package com.white.crackwifi.model;

import lombok.Data;
import lombok.ToString;

/**
 * @author huangzy
 * @Description: 单个WIFI对象
 * @date 2019/12/24 14:41
 */
@Data
@ToString
public class Ssid {

    //WIFI名称
    private String name;

    //是否需要密码
    private Boolean isPassword;

    //加密方式
    private String encryptionType;

    //信号强度
    private Integer signal;

//    //详细参数
//    private List<Bssid> bssidList;
//
//    /**
//     * 详细参数
//     */
//    public static class Bssid{
//
//        private String id;
//
//        //信号强度 0-100
//        private Integer signal;
//
//        //频道
//        private String channel;
//
//        //基本速率
//        private String rateOne;
//
//        //其他速率
//        private String rateTwo;
//    }
}
