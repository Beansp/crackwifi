package com.white.crackwifi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrackwifiApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrackwifiApplication.class, args);
    }

}
