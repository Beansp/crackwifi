package com.white.crackwifi.controller;

import com.white.crackwifi.exec.StartCrackWifi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huangzy
 * @Description:
 * @date 2019/12/25 15:20
 */
@RestController
@RequestMapping("/crackWifi")
public class CrackWifiController {

    @Autowired
    private StartCrackWifi startCrackWifi;

    /**
     * 开始破解WIFI
     */
    @RequestMapping("/start")
    public void start(){
        startCrackWifi.start();
    }

}
