package com.white.crackwifi.config;

import com.white.crackwifi.statics.TemplateData;
import com.white.crackwifi.utils.IOUtil;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

/**
 * @author huangzy
 * @Description: 初始化处理器
 * @date 2019/12/24 15:41
 */
@Component
public class SpringInitHandel implements ApplicationRunner {

    /**
     * 初始化
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        //将SSID模板放入缓存
        try {
            File ssidFileTemplate = new File(getClass().getClassLoader().getResource("SSID-template.xml").getPath());
            TemplateData.setSsidTemplateFile(ssidFileTemplate);
        } catch (Exception e) {
            System.out.println("SSID模板路径错误");
            e.printStackTrace();
        }
        //将密码字典信息放入缓存
        try {
            File passwordFile = new File(getClass().getClassLoader().getResource("password.txt").getPath());
            TemplateData.setPasswordData(IOUtil.fileToListString(passwordFile));
        } catch (Exception e) {
            System.out.println("密码路径路径错误");
            e.printStackTrace();
        }
    }
}
