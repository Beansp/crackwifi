package com.white.crackwifi;

import com.white.crackwifi.exec.StartCrackWifi;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CrackwifiApplicationTests {

    @Autowired
    private StartCrackWifi startCrackWifi;

    @Test
    void contextLoads() throws InterruptedException {
        startCrackWifi.start();
        Thread.sleep(1000000000L);
    }

}
